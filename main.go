package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
	"time"
)

var (
	start      string
	end        string
	timeFormat string
	dur        string
	sensor     string
)

func main() {
	lf, err := os.OpenFile("log.log", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		panic(err)
	}
	log.SetOutput(io.MultiWriter(os.Stderr, lf))
	log.SetFlags(log.Ldate | log.Lshortfile)
	flag.StringVar(&start, "start", "", "start time")
	flag.StringVar(&end, "end", "", "end time")
	flag.StringVar(&timeFormat, "tf", "2006-01-02 15:04:05", "time format")
	flag.StringVar(&dur, "dur", "", "duration")
	flag.StringVar(&sensor, "snif", "", "temperature sniffer file")
	flag.Parse()
	if sensor != "" {
		file, err := os.Open("/sys/class/thermal/" + sensor + "/temp")
		if err != nil {
			log.Fatalln(err)
		}
		tmp := make([]byte, 100)
		n, _ := file.Read(tmp)
		file.Close()
		file, err = os.OpenFile("temp.log", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
		fmt.Fprintf(file, "%s > %s\n", time.Now().Format(timeFormat), strings.TrimSpace(string(tmp[:n])))
		file.Close()
		return
	}
	var st, ed *time.Time = nil, nil
	if tmp, err := time.Parse(timeFormat, start); err != nil {
		if start != "" {
			log.Println(err)
		}
	} else {
		st = &tmp
	}
	if tmp, err := time.Parse(timeFormat, end); err != nil {
		if end != "" {
			log.Println(err)
		}
	} else {
		ed = &tmp
	}
	if tmp, err := time.ParseDuration(dur); err != nil {
		if dur != "" {
			log.Println(err)
		}
	} else {
		tmp1 := time.Now()
		ed = &tmp1
		tmp2 := tmp1.Add(-tmp)
		st = &tmp2
	}
	file, err := os.Open("temp.log")
	if err != nil {
		log.Fatalln(err)
	}
	scanner := bufio.NewScanner(file)
	result := struct {
		Min     float64
		Max     float64
		Current float64
		Sum     float64
		Count   int64
	}{Min: 999999999}
	for scanner.Scan() {
		res := strings.Split(scanner.Text(), " > ")
		if len(res) != 2 {
			continue
		}
		t, err := time.Parse(timeFormat, res[0])
		if err != nil {
			log.Println(err)
			continue
		}
		if st != nil && t.Before(*st) {
			continue
		}
		if ed != nil && t.After(*ed) {
			continue
		}
		d, err := strconv.ParseFloat(res[1], 64)
		if err != nil {
			log.Println(err)
			continue
		}
		d /= 1000
		result.Min = math.Min(result.Min, d)
		result.Max = math.Max(result.Max, d)
		result.Current = d
		result.Sum += d
		result.Count++
	}
	if result.Count > 0 {
		fmt.Printf("min %.3f max %.3f avg %.3f last %.3f\n", result.Min, result.Max, result.Sum/float64(result.Count), result.Current)
	} else {
		fmt.Println("no data")
	}
}
